const proxy = [
    {
      context: '/api',
      target: 'http://localhost:44319',
      pathRewrite: {'^/api' : ''}
    }
  ];
  module.exports = proxy;