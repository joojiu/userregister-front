export class User {
    id: number;
    name: string;
    code: number;
    type: string;
    age: number;
    active: boolean;
    dateBeginning: Date;
    dateEnding: Date;
}
