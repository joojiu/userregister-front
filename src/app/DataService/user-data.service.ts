import { Injectable } from '@angular/core';
import { User } from 'src/Model/User';
import { ROOT_URL } from 'src/Model/Config';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(private http: HttpClient) { }

  AddUser(user: User) {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const body = {
      Name: user.name,
      Code: user.code,
      Type: user.type,
      Age: user.age
    };
    return this.http.post<User>(ROOT_URL + '/User', body, { headers });
  }

  GetUserById(id: number) {
    return this.http.get<User>(ROOT_URL + '/User/' + id);
  }

  UpdateUser(user: User) {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const body = {
      Id: user.id,
      Name: user.name,
      Code: user.code,
      Type: user.type,
      Age: user.age
    };
    return this.http.put<User>(ROOT_URL + '/User/' + user.id, body, { headers });
  }

  DropUser(id: number) {
    return this.http.delete(ROOT_URL + '/User/' + id);
  }

  GetAll() {
    return this.http.get<User[]>(ROOT_URL + '/User');
  }
}
