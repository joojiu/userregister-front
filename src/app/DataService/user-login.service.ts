import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ROOT_URL } from 'src/Model/Config';
import { Login } from 'src/Model/Login';
import { Register } from 'src/Model/Register';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {

  constructor(private http: HttpClient) { }

  Login(login: Login) {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const body = {
      UserName: login.UserName,
      Password: login.Password
    };
    return this.http.post<Login>(ROOT_URL + '/Login', body, { headers });
  }

  CreateLogin(register: Register) {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    const body = {
      Name: register.name,
      LastName: register.lastName,
      Email: register.email,
      Password: register.password
    };

    return this.http.post<Register>(ROOT_URL + '/Register', body, { headers });
  }
}
