import { Component, OnInit, TemplateRef, Output, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { User } from 'src/Model/User';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {
  modalRef: BsModalRef;
  @Output() user = new EventEmitter();

  constructor(private modalService: BsModalService) {
  }

  ngOnInit() {
  }

  open(template: TemplateRef<any>) {
    console.log(template);
    this.modalRef = this.modalService.show(template);
  }

  onSubmit(event: NgForm) {
    console.log('It\'s valid?', event.valid);
    if (event.valid) {
      this.user.emit(event.value);
    }
  }

}
