import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/Model/User';
import { UserDataService } from '../DataService/user-data.service';
import { UserUpdateComponent } from '../user-update/user-update.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  users: User[];
  user: User;
  @ViewChild(UserUpdateComponent, {static: true}) updateModal: UserUpdateComponent;

  constructor(private dataservice: UserDataService) { }

  ngOnInit() {
    this.GetAll();
  }

  InsertUser(user: User) {
    this.dataservice.AddUser(user).subscribe(res => {
      console.log(res);
    });
    window.document.location.reload();
  }

  GetUserById(id: number) {
    this.dataservice.GetUserById(id).subscribe(res => {
      console.log(res);
      this.user = res;
      this.updateModal.openModal(this.user);
    });
  }

  UpdateUser(user: User) {
    console.log(user);
    this.dataservice.UpdateUser(user).subscribe(res => {
      console.log(res);
    });
    window.document.location.reload();
  }

  DeleteUser(id) {
    this.dataservice.DropUser(id).subscribe(res => {
      alert('Deletado com Sucesso!');
      window.document.location.reload();
    });
  }

  GetAll() {
    this.dataservice.GetAll().subscribe(res => {
      console.log(res);
      this.users = res;
    });
  }

}
