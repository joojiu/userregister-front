import { Component, OnInit } from '@angular/core';
import { Login } from 'src/Model/Login';
import { UserLoginService } from '../DataService/user-login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {

  userLogin: Login;
  errorMessage: string;

  constructor(private service: UserLoginService, private router: Router) { }

  ngOnInit() {
    this.userLogin = new Login();
  }

  login() {
    this.goToPage('/List');
    this.service.Login(this.userLogin).subscribe(res => {
      if (res) {
        console.log(res);
      }
    });
  }

  goToPage(pageName: string) {
    this.router.navigate([`${pageName}`]);
  }

}
