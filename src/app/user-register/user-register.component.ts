import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Register } from 'src/Model/Register';
import { UserLoginService } from '../DataService/user-login.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss']
})
export class UserRegisterComponent implements OnInit {
  userRegister = new FormGroup({
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', [ Validators.required, Validators.minLength(4), Validators.email ]),
    password: new FormControl('', [ Validators.required, Validators.minLength(6) ]),
    confirmPassword: new FormControl('')
  });
  notEqual: boolean;
  notLength: boolean;

  constructor(private service: UserLoginService) { }

  ngOnInit() {
  }

  onFormSubmit() {
    if (this.userRegister.get('confirmPassword').hasError('minLength')) {
      this.notLength = true;
    }
    if (!this.checkPassword()) {
      this.notEqual = true;
    } else {
      this.notEqual = false;
      this.notLength = false;

      const user = this.userRegister.value;
      this.createEmployee(user);
    }
  }

  createEmployee(register: Register) {
    this.service.CreateLogin(register);
  }

  checkPassword() {
    const password = this.userRegister.get('password');
    const confirmPassword = this.userRegister.get('confirmPassword');

    return password.value === confirmPassword.value;
  }

}
