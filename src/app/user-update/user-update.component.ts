import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { User } from 'src/Model/User';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {
  user: User;
  @Output() userResponse = new EventEmitter();
  @ViewChild(ModalDirective, {static: false}) modal: ModalDirective;

  constructor() { }

  ngOnInit() {
    this.user = new User();
  }

  openModal(user: User) {
    this.modal.show();
    this.user = user;
  }

  Save() {
    this.userResponse.emit(this.user);
  }
}
